<?xml version="1.0" encoding="UTF-8"?>
<clientConfig version="1.1">
    <emailProvider id="mailoo.org">
        <domain>mailoo.org</domain>
        <displayName>Mailoo.org</displayName>
        <displayShortName>Mailoo.org</displayShortName>
        <incomingServer type="imap">
            <hostname>mail.mailoo.org</hostname>
            <port>993</port>
            <socketType>SSL</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
        </incomingServer>
        <outgoingServer type="smtp">
            <hostname>mail.mailoo.org</hostname>
            <port>587</port>
            <socketType>STARTTLS</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
        </outgoingServer>
    </emailProvider>
</clientConfig>
